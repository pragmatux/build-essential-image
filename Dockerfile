FROM debian:bullseye
MAINTAINER pragmatux-users@lists.pragmatux.org

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
 && apt-get -yq install \
    build-essential \
    debhelper
